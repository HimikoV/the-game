extends TextureRect

signal fade_finished

func _input(event):
	if(event is InputEventKey):
		get_tree().change_scene("res://main_menu/Title_Screen.tscn")

func _on_AnimationPlayer_animation_finished(_anim_name):
	get_tree().change_scene("res://main_menu/Title_Screen.tscn")
	emit_signal("fade_finished")
