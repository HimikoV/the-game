extends Control

var scene_path_to_load

func _ready():
	var buttons = [$ScenesContainer/scene1/Scene1Button,$ScenesContainer/scene2/Scene1Button,$back_button]
	for button in buttons:
		button.connect("pressed", self, "_on_Button_pressed", [button.scene_to_load])
	


func _on_Button_pressed(scene_to_path):
	scene_path_to_load = scene_to_path
	$FadeIn.show()
	$FadeIn.fade_in()

func _on_FadeIn_fade_finished():
	get_tree().change_scene(scene_path_to_load)
